package ua.privatbank.secure.util;

import java.io.File;
import java.io.IOException;
import java.util.Properties;

import org.openqa.selenium.Capabilities;
import org.openqa.selenium.remote.DesiredCapabilities;

/**
 * Class that extracts properties from the prop file.
 */
public final class PropertyLoader {

    private static final String DEFAULT_PROPERTIES = "/prodhost.properties";

    private PropertyLoader() {
    }

    public static Capabilities loadCapabilities() throws IOException {
        return loadCapabilities(getApplicationPropertiesResource());
    }

    public static Capabilities loadCapabilities(final String fromResource) throws IOException {
        Properties props = new Properties();
        props.load(PropertyLoader.class.getResourceAsStream(fromResource));
        String capabilitiesFile = props.getProperty("capabilities");

        Properties capsProps = new Properties();
        capsProps.load(PropertyLoader.class.getResourceAsStream(capabilitiesFile));

        DesiredCapabilities capabilities = new DesiredCapabilities();
        for (String name : capsProps.stringPropertyNames()) {
            String value = capsProps.getProperty(name);
            if (value.toLowerCase().equals("true") || value.toLowerCase().equals("false")) {
                capabilities.setCapability(name, Boolean.valueOf(value));
            } else if (value.startsWith("file:")) {
                capabilities.setCapability(name,
                        new File(".", value.substring(5)).getCanonicalFile().getAbsolutePath());
            } else {
                capabilities.setCapability(name, value);
            }
        }
        return capabilities;
    }

    public static String loadProperty(final String name) throws IOException {
        return loadProperty(name, getApplicationPropertiesResource());
    }

    public static String loadProperty(final String name, final String fromResource) throws IOException {
        Properties props = new Properties();
        props.load(PropertyLoader.class.getResourceAsStream(fromResource));
        return props.getProperty(name);
    }

    private static String getApplicationPropertiesResource() {
        return System.getProperty("application.properties", DEFAULT_PROPERTIES);
    }
}