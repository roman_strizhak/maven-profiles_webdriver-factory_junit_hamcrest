package ua.privatbank.secure.util;

public enum Language {
    RUS("РУС"),
    UKR("УКР"),
    ENG("ENG");

    private final String lang;

    Language(final String lang) {
        this.lang = lang;
    }

    public String getLanguageText() {
        return lang;
    }
}
