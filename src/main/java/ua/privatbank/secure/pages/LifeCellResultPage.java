package ua.privatbank.secure.pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.PageFactory;

import ua.privatbank.secure.widgets.TransactionResultWidget;

public class LifeCellResultPage extends AbstractPage {

    public LifeCellResultPage(final WebDriver webDriver) {
        super(webDriver);
        PageFactory.initElements(webDriver, this);
    }

    public TransactionResultWidget getTransactionResultWidget() {
        return instantiateWidget(TransactionResultWidget.class);
    }
}
