package ua.privatbank.secure.pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.WebDriverWait;

/**
 * Abstract class representation of a AbstractPage in the UI. AbstractPage object pattern
 */
public abstract class AbstractPage {

    @FindBy(xpath = "//*[@class='preloader_form']")
    private WebElement preLoader;

    private WebDriverWait explicitWait;

    private static WebDriver driver;

    public AbstractPage(final WebDriver webDriver) {
        driver = webDriver;
    }

    public WebDriver getDriver() {
        return driver;
    }

    protected <R> R instantiateWidget(final Class<R> clazz) {
        return PageFactory.initElements(getDriver(), clazz);
    }

    public String getTitle() {
        return driver.getTitle();
    }

    public String getUrl() {
        return driver.getCurrentUrl();
    }

    public String getPageSource() {
        return driver.getPageSource();
    }

    public WebElement getActiveElementOnPage() {
        return driver.switchTo().activeElement();
    }

    public WebElement getPreLoader() {
        return preLoader;
    }

    public WebDriverWait getExplicitWait() {
        if (explicitWait == null) {
            explicitWait = new WebDriverWait(getDriver(), 30, 1000);
        }
        return explicitWait;
    }
}