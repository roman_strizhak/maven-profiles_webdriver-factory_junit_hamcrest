package ua.privatbank.secure.pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.PageFactory;

import ua.privatbank.secure.widgets.CardBoxWidget;
import ua.privatbank.secure.widgets.CompanyFooterWidget;
import ua.privatbank.secure.widgets.CompanyHeaderWidget;
import ua.privatbank.secure.widgets.InputBoxWidget;

public class LifeCellHomePage extends AbstractPage {

    public LifeCellHomePage(final WebDriver webDriver) {
        super(webDriver);
        PageFactory.initElements(webDriver, this);
    }

    public CompanyHeaderWidget getCompanyHeaderWidget() {
        return instantiateWidget(CompanyHeaderWidget.class);
    }

    public InputBoxWidget getInputBoxWidget() {
        return instantiateWidget(InputBoxWidget.class);
    }

    public CompanyFooterWidget getCompanyFooterWidget() {
        return instantiateWidget(CompanyFooterWidget.class);
    }

    public CardBoxWidget getCardBoxWidget() {
        return instantiateWidget(CardBoxWidget.class);
    }

}