package ua.privatbank.secure.widgets;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class CompanyFooterWidget extends AbstractWidget {

    @FindBy(xpath = "//*[@class='footer']/a")
    private WebElement liqpayLink;

    @FindBy(xpath = "//*[@class='footer']/img")
    private WebElement footerImage;

    public CompanyFooterWidget(final WebDriver driver) {
        super(driver);
    }

    public WebElement getLiqpayLink() {
        return liqpayLink;
    }

    public WebElement getFooterImage() {
        return footerImage;
    }
}