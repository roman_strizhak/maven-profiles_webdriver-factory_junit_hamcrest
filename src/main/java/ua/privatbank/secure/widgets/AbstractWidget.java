package ua.privatbank.secure.widgets;

import org.openqa.selenium.WebDriver;

/**
 * Abstract class representation of a AbstractWidget in some page. AbstractWidget object pattern
 */
public abstract class AbstractWidget {

    private WebDriver driver;

    public AbstractWidget(final WebDriver driver) {
        this.driver = driver;
    }

    protected WebDriver getDriver() {
        return driver;
    }
}
