package ua.privatbank.secure.widgets;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class TransactionResultWidget extends AbstractWidget {
    @FindBy(xpath = "//*[@class='reult_datashell_data']//*[@el='ResAmn']")
    private WebElement paymentAmountValue;

    @FindBy(xpath = "//*[@el='ResId']")
    private WebElement idTransactionValue;

    @FindBy(xpath = "//*[@el='ResPhone']")
    private WebElement phoneOrAccountValue;

    public TransactionResultWidget(final WebDriver driver) {
        super(driver);
    }

    public WebElement getPaymentAmountValue() {
        return paymentAmountValue;
    }

    public WebElement getIdTransactionValue() {
        return idTransactionValue;
    }

    public WebElement getPhoneOrAccountValue() {
        return phoneOrAccountValue;
    }
}
