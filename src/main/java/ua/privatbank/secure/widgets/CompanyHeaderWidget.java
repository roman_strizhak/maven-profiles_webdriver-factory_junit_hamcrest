package ua.privatbank.secure.widgets;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

import ua.privatbank.secure.util.Language;

public class CompanyHeaderWidget extends AbstractWidget {

    @FindBy(className = "logo")
    private WebElement pageLogo;

    @FindBy(css = "ul[el=Langbar]")
    private WebElement langBar;

    @FindBy(xpath = "//*[contains(@class, 'online_help_img_box')]")
    private WebElement supportButton;


    public CompanyHeaderWidget(final WebDriver driver) {
        super(driver);
    }

    public WebElement getPageLogo() {
        return pageLogo;
    }

    public WebElement getLangBar() {
        return langBar;
    }

    public WebElement getSupportButton() {
        return supportButton;
    }

    public WebElement getLanguageButton(final Language language) {
        return getLangBar().findElement(By.linkText(language.getLanguageText()));
    }
}