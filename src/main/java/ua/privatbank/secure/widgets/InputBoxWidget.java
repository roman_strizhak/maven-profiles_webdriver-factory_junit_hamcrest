package ua.privatbank.secure.widgets;

import java.util.List;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class InputBoxWidget extends AbstractWidget {

    @FindBy(id = "input_tel")
    private WebElement mobilePhoneInputField;

    @FindBy(xpath = "//*[@class='humane msg__error']")
    private WebElement errorMessagePopUp;

    @FindBy(xpath = "//*[@el='Billbox']//*[@for='phone']")
    private WebElement accountNumberRadioButtonToggle;

    @FindBy(xpath = "//*[@el='Radio_Tel']")
    private WebElement mobilePhoneNumberRadioButton;

    @FindBy(xpath = "//*[@el='Radio_Acc']")
    private WebElement accountNumberRadioButton;

    @FindBy(xpath = "//*[@el='Account']")
    private WebElement accountNumberInputField;

    @FindBy(id = "input_summ")
    private WebElement replenishmentAmountInputField;

    @FindBy(xpath = "//*[@class='summ-button-holder']/*[contains(@class, 'button')]")
    private List<WebElement> sumHolderButtons;

    public InputBoxWidget(final WebDriver driver) {
        super(driver);
    }

    public WebElement getMobilePhoneInputField() {
        return mobilePhoneInputField;
    }

    public WebElement getErrorMessagePopUp() {
        return errorMessagePopUp;
    }

    public WebElement getAccountNumberRadioButtonToggle() {
        return accountNumberRadioButtonToggle;
    }

    public WebElement getAccountNumberInputField() {
        return accountNumberInputField;
    }

    public List<WebElement> getPredefinedAmountButtons() {
        return sumHolderButtons;
    }

    public WebElement getAccountNumberRadioButton() {
        return accountNumberRadioButton;
    }

    public WebElement getMobilePhoneNumberRadioButton() {
        return mobilePhoneNumberRadioButton;
    }

    public WebElement getReplenishmentAmountInputField() {
        return replenishmentAmountInputField;
    }
}
