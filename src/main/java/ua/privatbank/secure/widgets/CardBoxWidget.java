package ua.privatbank.secure.widgets;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class CardBoxWidget extends AbstractWidget {

    @FindBy(xpath = "//*[@class='paycard_box_sender_date']//*[@el='SendMonth']")
    private WebElement payCardDateMonthDropDown;

    @FindBy(xpath = "//*[@class='paycard_box_sender_date']//*[@el='SendYear']")
    private WebElement payCardDateYearDropDown;

    @FindBy(xpath = "//*[contains(@class,'paycard_box_cvv_submit_btn')]")
    private WebElement payButton;

    @FindBy(xpath = "//*[@el='SendCard']")
    private WebElement cardInputField;

    @FindBy(xpath = "//*[@el='SendCVV']")
    private WebElement cardCVV;

    public CardBoxWidget(final WebDriver driver) {
        super(driver);
    }

    public WebElement getPayButton() {
        return payButton;
    }

    public WebElement getCardInputField() {
        return cardInputField;
    }

    public WebElement getPayCardDateYearDropDown() {
        return payCardDateYearDropDown;
    }

    public WebElement getPayCardDateMonthDropDown() {
        return payCardDateMonthDropDown;
    }

    public WebElement getCardCVV() {
        return cardCVV;
    }
}
