package ua.privatbank.secure.tests;

import java.util.Optional;

import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import ua.privatbank.secure.steps.LifeCellHomeActions;
import ua.privatbank.secure.steps.LifeCellHomeStates;
import ua.privatbank.secure.steps.LifeCellResultStates;
import ua.privatbank.secure.steps.SiteHeartActions;
import ua.privatbank.secure.steps.SiteHeartStates;
import ua.privatbank.secure.util.Language;

import static org.hamcrest.Matchers.allOf;
import static org.hamcrest.Matchers.both;
import static org.hamcrest.Matchers.containsInAnyOrder;
import static org.hamcrest.Matchers.containsString;
import static org.hamcrest.Matchers.endsWith;
import static org.hamcrest.Matchers.equalToIgnoringCase;
import static org.hamcrest.Matchers.greaterThan;
import static org.hamcrest.Matchers.hasItems;
import static org.hamcrest.Matchers.hasSize;
import static org.hamcrest.Matchers.is;
import static org.hamcrest.Matchers.isEmptyString;
import static org.hamcrest.Matchers.lessThan;
import static org.hamcrest.Matchers.not;
import static org.hamcrest.Matchers.startsWith;

public class LifeCellHomePageTests extends AbstractBaseTest {

    private LifeCellHomeActions lifeCellHomeActions;
    private LifeCellHomeStates lifeCellHomeStates;
    private LifeCellResultStates lifeCellResultStates;
    private SiteHeartActions siteHeartActions;
    private SiteHeartStates siteHeartStates;

    @Before
    public void beforeEachTest() {
        getDriver().get(getBaseUrl());
        lifeCellHomeActions = Optional.ofNullable(lifeCellHomeActions)
                .orElseGet(() -> new LifeCellHomeActions(getDriver()));
        lifeCellHomeStates = Optional.ofNullable(lifeCellHomeStates)
                .orElseGet(() -> new LifeCellHomeStates(getDriver()));
        lifeCellResultStates = Optional.ofNullable(lifeCellResultStates)
                .orElseGet(() -> new LifeCellResultStates(getDriver()));
        siteHeartActions = Optional.ofNullable(siteHeartActions).orElseGet(() -> new SiteHeartActions(getDriver()));
        siteHeartStates = Optional.ofNullable(siteHeartStates).orElseGet(() -> new SiteHeartStates(getDriver()));
    }

    @After
    public void afterEachTest() {
        getDriver().manage().deleteAllCookies();
    }

    @Test
    public void supportButtonTextHasText() {
        Assert.assertThat("Support button is wrong!", lifeCellHomeStates.getSupportButtonText(),
                containsString("Help"));
    }

    @Test
    public void supportButtonIsNotEmpty() {
        Assert.assertThat("Support button is empty!", lifeCellHomeStates.getSupportButtonText(), not(isEmptyString()));
    }

    @Test
    public void checkLogoLink() {
        Assert.assertThat("Logo link is wrong", lifeCellHomeStates.getLogoLink(), is("http://www.lifecell.com.ua/en/"));
    }

    @Test
    public void errorMessageForEmptyCardInputField() {
        lifeCellHomeActions.typeMobilePhoneNumber("380631112233");
        lifeCellHomeActions.typeReplenishmentAmount("10");
        lifeCellHomeActions.clickPayButton();
        Assert.assertThat("Error message is wrong!", lifeCellHomeStates.getErrorMessageText(),
                equalToIgnoringCase("Enter card"));
        final String redColor = "rgba(255, 0, 0, 1)";
        Assert.assertThat("Card field is not highlighted!", lifeCellHomeStates.getCardInputFieldBorderColor(),
                is(redColor));
    }

    @Test
    public void checkAccountNumberFieldIsEnable() {
        lifeCellHomeActions.clickAccountNumberRadioButton();
        Assert.assertTrue("AccountNumberInputField is disabled", lifeCellHomeStates.isAccountNumberInputFieldEnabled());
        Assert.assertFalse("MobilePhoneInputField is enabled", lifeCellHomeStates.isMobilePhoneInputFieldEnabled());
    }

    @Test
    public void checkHolderButtonsCount() {
        Assert.assertTrue("", lifeCellHomeStates.getSumHolderButtonsList().size() == 3);
        Assert.assertEquals("", 3, lifeCellHomeStates.getSumHolderButtonsList().size());
        Assert.assertThat("", lifeCellHomeStates.getSumHolderButtonsList().size(), is(3));
        Assert.assertThat("", lifeCellHomeStates.getSumHolderButtonsList(), hasSize(3));
    }

    @Test
    public void checkHolderButtonsText() {
        Assert.assertThat("", lifeCellHomeStates.getSumHolderButtonsTextList(), hasSize(3));
        Assert.assertThat("", lifeCellHomeStates.getSumHolderButtonsTextList(), hasItems("50", "100", "200"));
        Assert.assertThat("", lifeCellHomeStates.getSumHolderButtonsTextList(), containsInAnyOrder("100", "50", "200"));
    }

    @Test
    public void checkChangingSiteLanguage() {
        lifeCellHomeActions.changeSiteLanguage(Language.ENG);
        Assert.assertThat("Support button is wrong!", lifeCellHomeStates.getSupportButtonText(),
                containsString("Help"));
        Assert.assertThat("Logo link is wrong", lifeCellHomeStates.getLogoLink(), is("http://www.lifecell.com.ua/en/"));
    }

    @Test
    public void checkLifeCellPageTitle() {
        Assert.assertThat("Wrong title on the site!", lifeCellHomeStates.getTitleOfPage(), is("Lifecell"));
    }

    @Test
    public void checkChangeLinkAndTextOnSite() {
        lifeCellHomeActions.changeSiteLanguage(Language.ENG);
        Assert.assertThat("Link is wrong!", lifeCellHomeStates.getCurrentUrl(),
                allOf(startsWith("https:/"), containsString("en"), endsWith("lifecell")));
        Assert.assertThat("Text is wrong!", lifeCellHomeStates.getCurrentPageSource(),
                both(containsString("Top up account by payment card")).and(containsString(
                        "Here you can top-up mobile phone lifecell using a bank card VISA or MasterCard")));
    }

    @Test
    public void checkRightPlaceHolderOfNumberPhoneInput() {
        Assert.assertThat("Hint is wrong", lifeCellHomeStates.getActiveElementHint(), is("38 (093) XXX-XX-XX"));
        Assert.assertThat("First two digits are wrong!", lifeCellHomeStates.getHintInteger(),
                both(greaterThan(35)).and(lessThan(40)));
    }

    @Test
    public void checkElementsStatesOnPage() {
        Assert.assertFalse("AccountNumberRadioButton is checked!",
                lifeCellHomeStates.isAccountNumberRadioButtonSelect());
        Assert.assertTrue("MobilePhoneNumberRadioButton is not checked!",
                lifeCellHomeStates.isMobilePhoneNumberRadioButtonSelect());
        Assert.assertFalse("Account Number Field is enabled", lifeCellHomeStates.isAccountNumberInputFieldEnabled());
        Assert.assertThat("Color of pay button is not yellow!", lifeCellHomeStates.getPayButtonColor(),
                is("rgba(255, 221, 0, 1)"));
        Assert.assertTrue("Box for month is not displayed", lifeCellHomeStates.isPayCardSenderDateMonthDisplayed());
        Assert.assertTrue("Box for year is not displayed", lifeCellHomeStates.isPayCardSenderDateYearDisplayed());
    }

    @Test
    public void checkErrorMassageForWrongAccountNumber() {
        lifeCellHomeActions.clickAccountNumberRadioButton();
        lifeCellHomeActions.typeAccountNumber("123456");
        lifeCellHomeActions.clickPredefinedAmountButtonUsingForEach("50");
        lifeCellHomeActions.clickPredefinedAmountButton("100");
        lifeCellHomeActions.typeCardData("4249485180045920", "11/21", "123");
        lifeCellHomeActions.clickPayButton();
        Assert.assertThat("Error message is wrong", lifeCellHomeStates.getErrorMessageText(),
                equalToIgnoringCase("WRONG PHONE NUMBER OR PERSONAL ACCOUNT"));
        Assert.assertThat("ReplenishmentAmountValue is wrong", lifeCellHomeStates.getTypedReplenishmentAmountValue(),
                is("100"));
        Assert.assertThat("AccountNumberValue is wrong", lifeCellHomeStates.getTypedAccountNumberValue(), is("123456"));
    }
}