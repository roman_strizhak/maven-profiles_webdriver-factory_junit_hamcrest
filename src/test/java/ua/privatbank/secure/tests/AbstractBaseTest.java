package ua.privatbank.secure.tests;

import java.util.concurrent.TimeUnit;

import org.junit.ClassRule;
import org.junit.Rule;
import org.junit.rules.ExternalResource;
import org.openqa.selenium.Capabilities;
import org.openqa.selenium.WebDriver;

import ru.stqa.selenium.factory.WebDriverFactory;
import ua.privatbank.secure.util.PropertyLoader;

/**
 * Base class for all the JUnit-based test classes
 */
public abstract class AbstractBaseTest {

    private static String baseUrl;
    private static Capabilities capabilities;
    private static WebDriver driver;

    @ClassRule
    public static ExternalResource webDriverProperties = new ExternalResource() {
        @Override
        protected void before() throws Throwable {
            capabilities = PropertyLoader.loadCapabilities();
            baseUrl = new StringBuilder().append(PropertyLoader.loadProperty("site.url"))
                    .append(PropertyLoader.loadProperty("site.language"))
                    .append(PropertyLoader.loadProperty("branch.index"))
                    .append(PropertyLoader.loadProperty("company.name")).toString();
        };
    };

    @Rule
    public ExternalResource webDriver = new ExternalResource() {
        @Override
        protected void before() throws Throwable {
            if (capabilities.getBrowserName().equals("chrome")){
                System.setProperty("webdriver.chrome.driver", "chromedriver/chromedriver.exe");
            }
            driver = WebDriverFactory.getDriver(capabilities);
            driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
            driver.manage().window().maximize();
        };
    };

    protected String getBaseUrl() {
        return baseUrl;
    }

    protected WebDriver getDriver() {
        return driver;
    }
}