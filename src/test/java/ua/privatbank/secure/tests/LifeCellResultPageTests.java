package ua.privatbank.secure.tests;

import java.util.Optional;

import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import ua.privatbank.secure.steps.*;

import static org.hamcrest.Matchers.*;

public class LifeCellResultPageTests extends AbstractBaseTest {

    private LifeCellHomeActions lifeCellHomeActions;
    private LifeCellHomeStates lifeCellHomeStates;
    private LifeCellResultStates lifeCellResultStates;
    private SiteHeartActions siteHeartActions;
    private SiteHeartStates siteHeartStates;

    @Before
    public void beforeEachTest() {
        getDriver().get(getBaseUrl());
        lifeCellHomeActions = Optional.ofNullable(lifeCellHomeActions)
                .orElseGet(() -> new LifeCellHomeActions(getDriver()));
        lifeCellHomeStates = Optional.ofNullable(lifeCellHomeStates)
                .orElseGet(() -> new LifeCellHomeStates(getDriver()));
        lifeCellResultStates = Optional.ofNullable(lifeCellResultStates)
                .orElseGet(() -> new LifeCellResultStates(getDriver()));
        siteHeartActions = Optional.ofNullable(siteHeartActions).orElseGet(() -> new SiteHeartActions(getDriver()));
        siteHeartStates = Optional.ofNullable(siteHeartStates).orElseGet(() -> new SiteHeartStates(getDriver()));
    }

    @After
    public void afterEachTest() {
        getDriver().manage().deleteAllCookies();
    }

    @Test
    public void checkNotSuccefullPayment() {
        lifeCellHomeActions.typeMobilePhoneNumber("380631112233");
        lifeCellHomeActions.clickPredefinedAmountButton("100");
        lifeCellHomeActions.typeCardData("4249485180045920", "08/29", "456");
        lifeCellHomeActions.clickPayButton();
        Assert.assertTrue("Loader is not appeared", lifeCellHomeStates.isLoaderAppeared());
        Assert.assertTrue("loader is not dissapiared", lifeCellHomeStates.isLoaderDisappeared());
        Assert.assertThat("", lifeCellResultStates.getCurrentUrl(), endsWith("#result"));
        Assert.assertThat("", lifeCellResultStates.getTransactionAmount(), is("100"));
        Assert.assertThat("", lifeCellResultStates.getIdTransactionValue().length(), both(is(not(0))).and(is(8)));
        Assert.assertThat("", lifeCellResultStates.getPhoneNumberValue(), is("+380631112233"));
    }
}
