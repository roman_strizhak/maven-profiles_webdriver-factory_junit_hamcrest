package ua.privatbank.secure.tests;

import java.util.Optional;

import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import ua.privatbank.secure.steps.*;

import static org.hamcrest.Matchers.is;

public class LifeCellSupportWindowTests extends AbstractBaseTest {

    private LifeCellHomeActions lifeCellHomeActions;
    private LifeCellHomeStates lifeCellHomeStates;
    private LifeCellResultStates lifeCellResultStates;
    private SiteHeartActions siteHeartActions;
    private SiteHeartStates siteHeartStates;

    @Before
    public void beforeEachTest() {
        getDriver().get(getBaseUrl());
        lifeCellHomeActions = Optional.ofNullable(lifeCellHomeActions)
                .orElseGet(() -> new LifeCellHomeActions(getDriver()));
        lifeCellHomeStates = Optional.ofNullable(lifeCellHomeStates)
                .orElseGet(() -> new LifeCellHomeStates(getDriver()));
        lifeCellResultStates = Optional.ofNullable(lifeCellResultStates)
                .orElseGet(() -> new LifeCellResultStates(getDriver()));
        siteHeartActions = Optional.ofNullable(siteHeartActions).orElseGet(() -> new SiteHeartActions(getDriver()));
        siteHeartStates = Optional.ofNullable(siteHeartStates).orElseGet(() -> new SiteHeartStates(getDriver()));
    }

    @After
    public void afterEachTest() {
        getDriver().manage().deleteAllCookies();
    }

    @Test
    public void checkSwitchingAmongWindows() {
        String winHandleBefore = lifeCellHomeStates.getHandle();
        lifeCellHomeActions.clickHelpButton();
        lifeCellHomeActions.swithToNewWindow();
        siteHeartActions.createGETRequestAndPrintInConsole();
        siteHeartActions.closeWindow();
        lifeCellHomeActions.switchToWindow(winHandleBefore);
        Assert.assertThat("Link is wrong!", lifeCellHomeStates.getCurrentUrl(),
                is("https://secure.privatbank.ua/en/company/#lifecell"));
    }
}
