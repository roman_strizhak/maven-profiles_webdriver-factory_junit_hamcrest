package ua.privatbank.secure.steps;

import org.openqa.selenium.WebDriver;

import ua.privatbank.secure.pages.LifeCellResultPage;

public class LifeCellResultStates {

    private LifeCellResultPage lifeCellResultPage;

    public LifeCellResultStates(final WebDriver driver) {
        lifeCellResultPage = new LifeCellResultPage(driver);
    }

    public String getTransactionAmount() {
        return lifeCellResultPage.getTransactionResultWidget().getPaymentAmountValue().getText();
    }

    public String getIdTransactionValue() {
        return lifeCellResultPage.getTransactionResultWidget().getIdTransactionValue().getText();
    }

    public String getPhoneNumberValue() {
        return lifeCellResultPage.getTransactionResultWidget().getPhoneOrAccountValue().getText();
    }

    public String getCurrentUrl() {
        return lifeCellResultPage.getUrl();
    }
}
