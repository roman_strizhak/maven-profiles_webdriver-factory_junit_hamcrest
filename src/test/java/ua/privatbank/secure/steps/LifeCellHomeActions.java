package ua.privatbank.secure.steps;

import org.junit.Assert;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.Select;

import ua.privatbank.secure.pages.LifeCellHomePage;
import ua.privatbank.secure.util.Language;

public class LifeCellHomeActions {

    private LifeCellHomePage lifeCellHomePage;

    public LifeCellHomeActions(final WebDriver driver) {
        lifeCellHomePage = new LifeCellHomePage(driver);
    }

    public void typeMobilePhoneNumber(final String phoneNumber) {
        lifeCellHomePage.getInputBoxWidget().getMobilePhoneInputField().sendKeys(phoneNumber);
    }

    public void typeReplenishmentAmount(final String amount) {
        lifeCellHomePage.getInputBoxWidget().getReplenishmentAmountInputField().sendKeys(amount);
    }

    public void clickPayButton() {
        lifeCellHomePage.getCardBoxWidget().getPayButton().click();
    }

    public void clickAccountNumberRadioButton() {
        lifeCellHomePage.getInputBoxWidget().getAccountNumberRadioButtonToggle().click();
    }

    public void changeSiteLanguage(final Language language) {
        lifeCellHomePage.getCompanyHeaderWidget().getLanguageButton(language).click();
    }

    public void typeAccountNumber(final String number) {
        lifeCellHomePage.getInputBoxWidget().getAccountNumberInputField().sendKeys(number);
    }

    public void clickPredefinedAmountButtonUsingForEach(final String predefinedAmount) {
        for (WebElement predefinedAmountButton : lifeCellHomePage.getInputBoxWidget().getPredefinedAmountButtons()) {
            if (predefinedAmount.equals(predefinedAmountButton.getText())) {
                predefinedAmountButton.click();
                break;
            }
        }
    }

    public void clickPredefinedAmountButton(final String predefinedAmount) {
        switch (predefinedAmount) {
        case "50":
            clickPredefinedAmountButtonByIndex(1);
            break;
        case "100":
            clickPredefinedAmountButtonByIndex(2);
            break;
        case "200":
            clickPredefinedAmountButtonByIndex(3);
            break;
        default:
            // Assert.fail("Not found button with amount " + predefinedAmount);
            Assert.fail(String.format("Button with amount %s not found", predefinedAmount));
        }
    }

    private void clickPredefinedAmountButtonByIndex(final int index) {
        lifeCellHomePage.getInputBoxWidget().getPredefinedAmountButtons().get(index - 1).click();
    }

    public void typeCardNumber(final String cardNumber) {
        lifeCellHomePage.getCardBoxWidget().getCardInputField().sendKeys(cardNumber);
    }

    public void typePayCardDateMonth(final String month) {
        new Select(lifeCellHomePage.getCardBoxWidget().getPayCardDateMonthDropDown()).selectByVisibleText(month);
    }

    public void typePayCardDateYear(final String year) {
        new Select(lifeCellHomePage.getCardBoxWidget().getPayCardDateYearDropDown()).selectByVisibleText(year);
    }

    public void typePayCardCVV(final String cvv) {
        lifeCellHomePage.getCardBoxWidget().getCardCVV().sendKeys(cvv);
    }

    public void typeCardData(final String cardNumber, final String cardExpiredDate, final String cvv) {
        typeCardNumber(cardNumber);
        String cardMonth = cardExpiredDate.split("/")[0];
        typePayCardDateMonth(cardMonth);
        String cardYear = cardExpiredDate.split("/")[1];
        typePayCardDateYear(cardYear);
        typePayCardCVV(cvv);
    }

    public void clickHelpButton() {
        lifeCellHomePage.getCompanyHeaderWidget().getSupportButton().click();
    }

    public void swithToNewWindow() {
        for (String winHandle : lifeCellHomePage.getDriver().getWindowHandles()) {
            lifeCellHomePage.getDriver().switchTo().window(winHandle);
        }
    }

    public void switchToWindow(final String handle) {
        lifeCellHomePage.getDriver().switchTo().window(handle);
    }
}