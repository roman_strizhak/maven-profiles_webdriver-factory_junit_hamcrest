package ua.privatbank.secure.steps;

import java.util.HashMap;
import java.util.Map;

import org.openqa.selenium.WebDriver;

import ua.privatbank.secure.pages.SiteHeartPage;

public class SiteHeartActions {

    private SiteHeartPage siteHeartPage;

    public SiteHeartActions(final WebDriver driver) {
        siteHeartPage = new SiteHeartPage(driver);
    }

    private String getChangedCurrentUrl() {
        return siteHeartPage.getUrl().replace("%3A", ":").replaceAll("%2F", "/");
    }

    public void createGETRequestAndPrintInConsole() {
        Map<String, String> hashMap = new HashMap<String, String>();
        String allUrlParametres = getChangedCurrentUrl().split("\\?")[1];
        String[] requestParametersArray = allUrlParametres.split("&");
        for (int i = 0; i < requestParametersArray.length; i++) {
            String[] keyAndValueArray = requestParametersArray[i].split("=");
            hashMap.put(keyAndValueArray[0], keyAndValueArray[1]);
        }
        System.out.println(hashMap.toString());
    }

    public void closeWindow() {
        siteHeartPage.getDriver().close();
    }
}
