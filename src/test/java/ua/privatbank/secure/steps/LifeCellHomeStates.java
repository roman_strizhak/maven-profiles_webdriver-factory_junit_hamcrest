package ua.privatbank.secure.steps;

import java.util.LinkedList;
import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;

import ua.privatbank.secure.pages.LifeCellHomePage;

public class LifeCellHomeStates {

    private LifeCellHomePage lifeCellHomePage;

    public LifeCellHomeStates(final WebDriver driver) {
        lifeCellHomePage = new LifeCellHomePage(driver);
    }

    public String getSupportButtonText() {
        return lifeCellHomePage.getCompanyHeaderWidget().getSupportButton().getText();
    }

    public String getLogoLink() {
        return lifeCellHomePage.getCompanyHeaderWidget().getPageLogo().getAttribute("href");
    }

    public String getErrorMessageText() {
        return lifeCellHomePage.getInputBoxWidget().getErrorMessagePopUp().getText();
    }

    public String getCardInputFieldBorderColor() {
        return lifeCellHomePage.getCardBoxWidget().getCardInputField().getCssValue("border-top-color");
    }

    public boolean isAccountNumberInputFieldEnabled() {
        return lifeCellHomePage.getInputBoxWidget().getAccountNumberInputField().isEnabled();
    }

    public boolean isMobilePhoneInputFieldEnabled() {
        return lifeCellHomePage.getInputBoxWidget().getMobilePhoneInputField().isEnabled();
    }

    public List<WebElement> getSumHolderButtonsList() {
        return lifeCellHomePage.getInputBoxWidget().getPredefinedAmountButtons();
    }

    public List<String> getSumHolderButtonsTextList() {
        List<String> sumHolderButtonsTextList = new LinkedList<>();
        for (WebElement sumHolderButton : getSumHolderButtonsList()) {
            sumHolderButtonsTextList.add(sumHolderButton.getText());
        }
        return sumHolderButtonsTextList;
    }

    public String getTitleOfPage() {
        return lifeCellHomePage.getTitle();
    }

    public String getCurrentUrl() {
        return lifeCellHomePage.getUrl();
    }

    public String getCurrentPageSource() {
        return lifeCellHomePage.getPageSource();
    }

    public String getActiveElementHint() {
        return lifeCellHomePage.getActiveElementOnPage().getAttribute("placeholder");
    }

    public int getHintInteger() {
        return Integer.parseInt(getActiveElementHint().substring(0, 2));
    }

    public boolean isAccountNumberRadioButtonSelect() {
        return lifeCellHomePage.getInputBoxWidget().getAccountNumberRadioButton().isSelected();
    }

    public boolean isMobilePhoneNumberRadioButtonSelect() {
        return lifeCellHomePage.getInputBoxWidget().getMobilePhoneNumberRadioButton().isSelected();
    }

    public String getPayButtonColor() {
        return lifeCellHomePage.getCardBoxWidget().getPayButton().getCssValue("background-color");
    }

    public boolean isPayCardSenderDateMonthDisplayed() {
        return lifeCellHomePage.getCardBoxWidget().getPayCardDateMonthDropDown().isDisplayed();
    }

    public boolean isPayCardSenderDateYearDisplayed() {
        return lifeCellHomePage.getCardBoxWidget().getPayCardDateYearDropDown().isDisplayed();
    }

    public boolean isPreLoaderDisplay() {
        return lifeCellHomePage.getPreLoader().isDisplayed();
    }

    public String getTypedReplenishmentAmountValue() {
        return lifeCellHomePage.getInputBoxWidget().getReplenishmentAmountInputField().getAttribute("value");
    }

    public String getTypedAccountNumberValue() {
        return lifeCellHomePage.getInputBoxWidget().getAccountNumberInputField().getAttribute("value");
    }

    public boolean isLoaderAppeared() {
        lifeCellHomePage.getExplicitWait().until(ExpectedConditions.visibilityOf(lifeCellHomePage.getPreLoader()));
        return true;
    }

    public boolean isLoaderDisappeared() {
        lifeCellHomePage.getExplicitWait().until(ExpectedConditions.invisibilityOfElementLocated(By.xpath("//*[@class='preloader_form']")));
        return true;
    }

    public String getHandle() {
        return lifeCellHomePage.getDriver().getWindowHandle();
    }
}
