package ua.privatbank.secure;

import org.junit.Rule;
import org.junit.rules.ExternalResource;
import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;

import ru.stqa.selenium.factory.WebDriverFactory;
import ru.stqa.selenium.factory.WebDriverFactoryMode;
import ua.privatbank.secure.tests.LifeCellHomePageTests;
import ua.privatbank.secure.tests.LifeCellResultPageTests;
import ua.privatbank.secure.tests.LifeCellSupportWindowTests;

@RunWith(Suite.class)
@SuiteClasses({ LifeCellHomePageTests.class, LifeCellResultPageTests.class, LifeCellSupportWindowTests.class })
public class TestSuite {

    @Rule
    public ExternalResource webDriverFactory = new ExternalResource() {
        @Override
        protected void before() throws Throwable {
            WebDriverFactory.setMode(WebDriverFactoryMode.THREADLOCAL_SINGLETON);
        };

        @Override
        protected void after() {
            WebDriverFactory.dismissAll();
        };
    };
}